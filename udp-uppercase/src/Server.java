import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {
    public static void main(String[] args) throws IOException {
        byte[] received = new byte[1024];
        byte[] send = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(received,received.length);
        DatagramPacket sendPacket = new DatagramPacket(send,send.length);
        DatagramSocket datagramSocket = new DatagramSocket(5000);

            datagramSocket.receive(receivePacket);
            String data = new String(receivePacket.getData());
            data = data.toUpperCase();
            send = data.getBytes();

            sendPacket.setPort(receivePacket.getPort());
            sendPacket.setAddress(receivePacket.getAddress());
            sendPacket.setData(send);
            datagramSocket.send(sendPacket);
            datagramSocket.close();
    }
}
