import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client {
    public static void main(String[] args) throws IOException {
        byte[] received = new byte[1024];
        byte[] send;
        send = args[0].getBytes();
        int port = 5000;//Integer.parseInt(args[1]);

        InetAddress inetAddress = InetAddress.getByName("localhost");

        DatagramPacket receivePacket = new DatagramPacket(received,received.length);
        DatagramPacket sendPacket = new DatagramPacket(send,send.length,inetAddress,port);

        DatagramSocket datagramSocket = new DatagramSocket(sendPacket.getPort()+1);

        datagramSocket.send(sendPacket);

        receivePacket.setPort(sendPacket.getPort());
        receivePacket.setAddress(sendPacket.getAddress());
        datagramSocket.receive(receivePacket);

        String s = new String(receivePacket.getData());
        System.out.println(s);
        datagramSocket.close();

    }
}
