public class Bank {
    private double balance;

    public Bank(double amount){
        this.balance = amount;
    }

    public double withdraw(double amount) {
        if (amount <= balance) {
            balance -= amount;
            System.out.println("Operation successful, new balance is: "+this.balance);
            return amount;
        }else{
            System.out.println("Operation denied! Insufficient funds.");
        }
        return 0;
    }
    public double deposit(double amount){
            balance += amount;
            System.out.println("Deposit of " + amount + " confirmed. New balance is: " + this.balance);
        return amount;
    }
    public double getBalance(){
        return this.balance;
    }
}
