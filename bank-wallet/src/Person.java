public class Person {
    private Bank bankAccount;
    private Wallet wallet;

    public Person(Bank bankAccount, Wallet wallet){
        this.bankAccount = bankAccount;
        this.wallet = wallet;
    }

    public void withdraw(double amount){
        wallet.deposit(bankAccount.withdraw(amount));
    }
    public void deposit(double amount){
        if(wallet.getCash()>=amount){
            wallet.withdraw(bankAccount.deposit(amount));
        }
    }
    public double getCash(){
        return wallet.getCash();
    }
    public double getBalance(){
        return bankAccount.getBalance();
    }
}
