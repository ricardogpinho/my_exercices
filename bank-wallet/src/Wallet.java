public class Wallet {
    private double cash;

    public Wallet(double cash){
        this.cash = cash;
    }
    public double withdraw(double amount){
        if(this.cash>=amount){
            this.cash-=amount;
        }
        return amount;
    }
    public double deposit(double amount){
        this.cash += amount;
        return amount;
    }
    public double getCash(){
        return this.cash;
    }
}
