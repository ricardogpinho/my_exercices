public class Main {
    public static void main(String[] args) {
        Wallet wallet = new Wallet(10);
        Bank bankAccount = new Bank(100);
        Person person = new Person(bankAccount,wallet);

        person.deposit(20);
        person.withdraw(500);
        System.out.println(person.getCash());
        System.out.println(person.getBalance());
        /*person.deposit(7);
        person.withdraw(20);
        System.out.println(person.getCash());
        System.out.println(person.getBalance());*/

    }
}
