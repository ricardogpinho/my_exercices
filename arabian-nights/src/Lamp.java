public class Lamp {
    private int maxGenies;
    private int remainingGenies;

    public Lamp(){
        this.maxGenies = 1+(int)Math.floor(Math.random()*3);
        this.remainingGenies = maxGenies;
        System.out.println("You have "+remainingGenies+ " Genies");
    }

    public Genie rubLamp(){
        System.out.println("You rubbed a lamp.");
        if(remainingGenies<=0){
            remainingGenies--;
            return new DemonGenie();
        }else {
            if((maxGenies-remainingGenies)%2==0){
                remainingGenies--;
                return new FriendlyGenie();
            }else if(maxGenies-remainingGenies%2!=0){
                remainingGenies--;
                return new GrumpyGenie();
            }
        }
        return new Genie();
    }


}
