public class Desert {
    public static void main(String[] args) {
        Lamp lamp = new Lamp();
        Genie genie = new Genie();
        genie = lamp.rubLamp();
        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
        genie = lamp.rubLamp();
        genie.grantWish();
        genie.grantWish();
        genie.grantWish();
    }
}
