public class DemonGenie extends Genie {
    public DemonGenie(){
        super();
    }

    @Override
    public void grantWish() {
        super.setWishesRemaining(super.getMaxWishes());
        System.out.println("Demon Genie: Wish granted!");
    }
}
