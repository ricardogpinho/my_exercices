public class Genie {
    private int maxWishes;
    private int wishesRemaining;
    public Genie(){
        this.maxWishes = 1+(int)Math.floor(Math.random()*3);
            wishesRemaining=maxWishes;
    }
    public void grantWish(){
        if(wishesRemaining>0) {
            wishesRemaining--;
            System.out.println("Friendly Genie: Wish granted! "+"You have "+this.wishesRemaining+" wishes left");
        }else{
            System.out.println("Friendly Genie: You outta wishes");
        }
    }
    public int getMaxWishes(){
        return maxWishes;
    }
    public int getWishesRemaining(){
        return wishesRemaining;
    }
    public void setWishesRemaining(int wishesRemaining){
        this.wishesRemaining=wishesRemaining;
    }
}
