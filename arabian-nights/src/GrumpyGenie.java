public class GrumpyGenie extends Genie{
    public GrumpyGenie(){
        super();
    }

    @Override
    public void grantWish() {
        if(super.getWishesRemaining()>0){
            System.out.println("Grumpy Genie: Wish granted!");
            super.setWishesRemaining(0);
        }
    }
}
