/**
 * Convert a binary String to a Number
 */
exports.binaryToDecimal = function(str) {
   return parseInt(str,2);

};

/**
 * Add two Numbers with a precision of 2
 */
exports.add =  function(a, b) {
   a = a*100;
   b= b*100;
   var r = (a+b)/100;
   return r;
};

/**
 * Multiply two Numbers with a precision of 4
 */
exports.multiply =  function(a, b) {
   a = a*10000;
   b = b*10000;
   var r = a*b/100000000
   return r;
};
