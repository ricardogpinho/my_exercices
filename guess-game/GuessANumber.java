public class GuessANumber {
	public static void start(Player[] players, int min, int max){
		int numberToGuess = Randomizer.randomize(min,max);
		System.out.println(numberToGuess+ " is the number to guess!");
		int playerPick;
		for(int i = 0; i < players.length ; i++){
			playerPick = players[i].pickANumber(min,max);
			if (playerPick==numberToGuess){
				System.out.println(players[i].getName()+" guessed "+playerPick+" and it's right!");
				break;
			}else{
				System.out.println(players[i].getName()+" guessed "+playerPick+" and it's wrong!");
			}
		}
	}
}
