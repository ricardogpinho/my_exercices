public class Player {
	private String name;
	public Player(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public static int pickANumber(int min, int max){
		return Randomizer.randomize(min,max);
	}
}
