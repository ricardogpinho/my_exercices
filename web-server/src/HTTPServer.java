import javax.swing.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class HTTPServer {
    private ServerSocket serverSc;
    private Socket sc;

    public void init(int port){
        try {
           serverSc = new ServerSocket(port);
       }catch (IOException ex){
           System.out.println("Couldn't start server");
       }
    }
    public void start(){
        while(true) {
            try {
                sc = serverSc.accept();
                InputStreamReader inputStreamReader = new InputStreamReader(sc.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder request = new StringBuilder();
                String temp;
                while(!(temp = bufferedReader.readLine()).startsWith("Accept-Language")) {
                    request.append(temp);
                    System.out.println(temp);
                }

                readRequest(request.toString());

            } catch (IOException ex) {
                System.out.println("Unable to create communication socket");
            }
        }
    }
    private void readRequest(String request){
        String[] request_values = request.split(" ");
        switch (request_values[0]){
            case ("GET"):
                sendBack(request_values[1]);
                break;

        }
    }
    private void sendBack(String resource) {
        if (resource.equals("/")) {
            resource = "index.html";
        }
        try {
            File f = getFile(resource);
            DataOutputStream dOut = new DataOutputStream(sc.getOutputStream());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(f));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public File getFile(String file){
        return new File(file);
    }
}
