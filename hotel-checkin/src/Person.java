public class Person {
    Hotel hotel;
    private int roomKey;
    public Person(Hotel hotel){
        this.hotel = hotel;
    }
    public void checkIn(){
        this.roomKey = hotel.checkIn();
    }
    public void checkOut(){
        hotel.checkOut(roomKey);
    }
}
