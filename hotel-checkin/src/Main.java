public class Main {
    public static void main(String[] args) {
        Hotel hotel = new Hotel(3);
        Person person = new Person(hotel);
        Person person2 = new Person(hotel);
        Person person3 = new Person(hotel);
        Person person4 = new Person(hotel);
        Person person5 = new Person(hotel);

        person.checkIn();
        person2.checkIn();
        person3.checkIn();
        person2.checkOut();
        person4.checkIn();
        person5.checkIn();
        person.checkOut();
        person5.checkIn();
    }
}
