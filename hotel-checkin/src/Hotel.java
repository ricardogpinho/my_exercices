public class Hotel {
    private Room[] rooms;
    public Hotel(int nRooms){
        rooms = new Room[nRooms];
        for (int i = 0; i < rooms.length; i++) {
            rooms[i] = new Room(i+1,false);
        }
    }
    public int checkIn(){
        int[] emptyRooms = emptyRooms();
        if(emptyRooms.length>0) {
            System.out.println("\nOs seguintes quartos estão disponiveis: ");
            listEmptyRooms(emptyRooms);
            System.out.println("Vou entregar o quarto nº" + emptyRooms[0]+"\n");
            rooms[emptyRooms[0]-1].setOccupied(true);
            return emptyRooms[0];
        }else {
            System.out.println("Lamentamos mas não existem quartos vagos.");
        }
        return 0;
    }

    private void listEmptyRooms(int[] emptyRooms) {
        for(int i=0; i<emptyRooms.length;i++){
            System.out.print(emptyRooms[i]+", ");
        }
        System.out.println();
    }
    public void checkOut(int roomKey){
        rooms[roomKey-1].setOccupied(false);
        System.out.println("Check-out no quarto "+roomKey+". O quarto agora está livre");
    }
    private int[] emptyRooms() {
        int[] emptyRooms = {};
        for (int i = 0; i < rooms.length; i++) {
            if (!rooms[i].isOccupied()) {
                emptyRooms = addArray(emptyRooms.length, emptyRooms, rooms[i].getNumber());
            }
        }
        return emptyRooms;
    }
    public static int[] addArray(int n, int arr[], int x) {
        int newArr[] = new int[n + 1];
        for (int i = 0; i < n; i++) {
            newArr[i] = arr[i];
        }
        newArr[n] = x;
        return newArr;
    }

}

