public class Room {
    private int roomNumber;
    private boolean isOccupied;
    public Room(int roomNumber, boolean isOccupied){
        this.roomNumber = roomNumber;
        this.isOccupied = false;
    }
    public boolean isOccupied(){
        return this.isOccupied;
    }
    public int getNumber(){
        return this.roomNumber;
    }
    public void setOccupied(boolean occupied){
        this.isOccupied = occupied;
    }
}
