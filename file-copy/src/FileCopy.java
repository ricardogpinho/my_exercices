import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;

    public FileCopy(){

    }

    public void copy(String original, String clone) throws IOException {
        fileInputStream = new FileInputStream(original);
        fileOutputStream = new FileOutputStream(clone);
        int buffer = fileInputStream.read();
        while(buffer!=-1) {
            fileOutputStream.write(buffer);
            buffer = fileInputStream.read();
        }
    }
}
