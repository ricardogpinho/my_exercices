public class Main {
    private static final String TEXT;
    public static void main(String[] args) {
        WordHistogramComp wordHistogram = new WordHistogramComp();
        wordHistogram.insert(TEXT);

        System.out.println("The map has "+wordHistogram.size()+" distinct words.\n");

        for(String word : wordHistogram){
            System.out.println(word+": "+wordHistogram.get(word));
        }
    }
}

