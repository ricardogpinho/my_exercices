package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.cars.CarType;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class Game {
    public static final int MANUFACTURED_CARS = 20;
    private Car[] cars;
    private final int delay;

    public Game(int cols, int rows, int delay) {
        Field.init(cols, rows);
        this.delay = delay;
    }

    // Creates a bunch of cars and randomly puts them in the field
    public void init() {
        cars = new Car[MANUFACTURED_CARS];
        cars[0]= CarFactory.getTesla();
        for (int i = 1; i < cars.length; i++) {
            cars[i] = CarFactory.getRandomNewCar();
        }
        Field.draw(cars);
    }

    //Starts the animation, @throws InterruptedException
    public void start() throws InterruptedException {
        while (true) {
            Thread.sleep(delay);
            moveAllCars();
            Field.draw(cars);
        }
    }
    private void moveAllCars() {
        for (Car car:cars){
            if(car.getCarType()==CarType.TESLA) {
                advancedTeslaCollisionDetector(car);
            }
            collisionDetector(car);
            car.followRoute();
        }
    }
    public void advancedTeslaCollisionDetector(Car car){
        boolean collision = false;
        do{
        for(int i = 0; i<cars.length;i++){
            collision=false;
            if(car==cars[i]){continue;}
            if(car.getNextPosition().getRow()==cars[i].getNextPosition().getRow()&&
               car.getNextPosition().getCol()==cars[i].getNextPosition().getCol()) {
                collision = true;
                car.newRoute();
                break;
            }
            if(car.getNextPosition().getRow()==cars[i].getPos().getRow()&&
               car.getNextPosition().getCol()==cars[i].getPos().getCol()) {
                collision = true;
                car.newRoute();
                break;
            }
        }
        }while(collision == true);
        car.drawRoute();

    }
    private void collisionDetector(Car car){
        for(Car _car : cars){
            if(car==_car){continue;}
            if(carsInSamePosition(car, _car)) {
                if ((isAmbulance(car)&&!car.isCrashed() || isAmbulance(_car)&&!_car.isCrashed()) && !isPanzer(car) && !isPanzer(_car)) {
                    _car.setCrashed(false);
                    car.setCrashed(false);
                } else {
                    car.setCrashed(!isPanzer(car));
                    _car.setCrashed(!isPanzer(_car));
                }
            }
        }
    }
   private boolean carsInSamePosition(Car car, Car _car){
        return (car.getPos().getCol()==_car.getPos().getCol()&&car.getPos().getRow()==_car.getPos().getRow());
    }
    private boolean isPanzer(Car car){
        return car.getCarType()==CarType.PANZER;
    }
    private boolean isAmbulance(Car car){
        return car.getCarType()==CarType.AMBULANCE;
    }
}
