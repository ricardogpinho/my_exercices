package org.academiadecodigo.carcrash.cars;
import org.academiadecodigo.carcrash.field.*;

public class Route {
    private Position[] route;
    private Position initialPosition;
    private Position finalPosition;
    private int currentPosition;

    public Route() {
        this.initialPosition = Position.generateRandomPosition();
        this.finalPosition = Position.generateRandomPosition();
        this.currentPosition = 0;
        traceRoute();
    }
    public void newRoute(){
        initialPosition = route[currentPosition].getPosition();
        finalPosition = Position.generateRandomPosition();
        currentPosition = 0;
        traceRoute();
    }
    private void traceRoute() {
        route = new Position[_rowDif() + _colDif() + 1];
        route[0] = initialPosition;
        int rnd = (int)Math.ceil(Math.random()*2);
        for (int i = 1; i < route.length; i++) {
            route[i] = route[i - 1].getPosition();
            switch (rnd) {
                case 1:
                    if (_isEast(route[i]) && route[i].getCol() != finalPosition.getCol()) {
                        route[i].goEast();
                    } else if (!_isEast(route[i]) && route[i].getCol() != finalPosition.getCol()) {
                        route[i].goWest();
                    } else if (_isSouth(route[i])) {
                        route[i].goSouth();
                    } else {
                        route[i].goNorth();
                    }
                    break;
                case 2:
                    if (!_isSouth(route[i]) && route[i].getRow() != finalPosition.getRow()) {
                        route[i].goNorth();
                    } else if (_isSouth(route[i]) && route[i].getRow() != finalPosition.getRow()) {
                        route[i].goSouth();
                    } else if (!_isEast(route[i])) {
                        route[i].goWest();
                    } else {
                        route[i].goEast();
                    }
                    break;
            }

        }
      //Field.drawRoute(route);
    }
    public void drawRoute(){
        Field.drawRoute(this.route,this.currentPosition);
    }
    public Position followRoute() {
        if (currentPosition == route.length - 1) {
           newRoute();
        }
        return route[currentPosition++];
    }

    public Position getNextPosition(){
        if(currentPosition < route.length-1){
            return route[currentPosition+1];
        }
        return new Position(-1,-1);
    }
    private boolean _isSouth(Position position) {
        return (finalPosition.getRow() > position.getRow());
    }
    private boolean _isEast(Position position) {
        return (finalPosition.getCol() > position.getCol());
    }
    private int _rowDif() {
        return Math.abs(finalPosition.getRow() - initialPosition.getRow());
    }
    private int _colDif() {
        return Math.abs(finalPosition.getCol() - initialPosition.getCol());
    }
}
