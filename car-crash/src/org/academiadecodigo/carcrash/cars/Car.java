package org.academiadecodigo.carcrash.cars;

import com.googlecode.lanterna.terminal.Terminal.Color;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {
    CarType cartype;
    private Position position;
    private boolean crashed;
    private Route route;
    private int speed;
    private int _speedCount = 1;
    public Color forColor;
    public Color bacColor;


    public void initiateCarProperties(){
                this.forColor = cartype.getForColor();
                this.bacColor = cartype.getBacColor();
                this.speed = cartype.getSpeed();
                this.crashed = false;
    }
    public void newRoute(){
        route.newRoute();
    }
    public void drawRoute(){
        route.drawRoute();
    }

    public Car(CarType carType){
        this.cartype = carType;
        initiateCarProperties();
        route = new Route(); //the route is created
        position = route.followRoute();//enter first step in the route
    }

    public void followRoute(){
        if(crashed) {return;};
        if(_speedCount<speed){
            _speedCount++;
            return;
        }
        this.position = route.followRoute();
        _speedCount = 1;
    }
    public Position getNextPosition(){
        return route.getNextPosition();
    }
    public Position getPos() {
        return position;
    }
    public void setCrashed(boolean crashed){
        this.crashed=crashed;
    }
    public boolean isCrashed() {
        return crashed;
    }
    public CarType getCarType(){
        return cartype;
    }
    @Override
    public String toString() {
        return cartype.getSymbol();
    }

}
