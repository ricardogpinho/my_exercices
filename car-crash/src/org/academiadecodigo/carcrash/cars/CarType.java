package org.academiadecodigo.carcrash.cars;
import static com.googlecode.lanterna.terminal.Terminal.Color.*;
import static com.googlecode.lanterna.terminal.Terminal.*;
public enum CarType {
    //BRAND(speed,symbol,color_text,color_background) -speed variable, lower is faster, 1 is max speed possible-
    FIAT(3, "f",WHITE,BLUE),
    FERRARI(1, "F",RED,YELLOW),
    PANZER(5, "T",BLACK,GREEN),
    AMBULANCE(2, "+",RED,WHITE),
    TESLA(1,"T",BLACK,WHITE);

    private final int speed;
    private final String symbol;
    private final Color forColor;
    private final Color bacColor;

    CarType(int speed, String symbol, Color forColor, Color bacColor){
        this.speed = speed;
        this.symbol = symbol;
        this.forColor = forColor;
        this.bacColor = bacColor;
    }
    public Color getForColor(){
        return this.forColor;
    }
    public Color getBacColor(){
        return this.bacColor;
    }
    public int getSpeed(){
        return this.speed;
    }
    public String getSymbol(){
        return this.symbol;
    }
}
