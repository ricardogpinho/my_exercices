package org.academiadecodigo.carcrash.cars;

public class Ambulance extends Car{
    public Ambulance(){
        super(CarType.AMBULANCE);
    }
}