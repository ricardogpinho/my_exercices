package org.academiadecodigo.carcrash.cars;

public class CarFactory {

    public static Car getRandomNewCar() {
        int rnd = (int) Math.ceil(Math.random() * CarType.values().length - 2);
        switch (rnd) {
            case 0:
                return new Fiat();
            case 1:
                return new Ferrari();
            case 2:
                return new Panzer();
            case 3:
                return new Ambulance();
            case 4:
                return new Tesla();
            default:
                return new Ambulance();
        }
    }

    public static Car getTesla(){
        return new Tesla();
    }
    public static Car getFiat() {
        return new Fiat();
    }

    public static Car getFerrari() {
        return new Ferrari();
    }

    public static Car getPanzer() {
        return new Panzer();
    }

    public static Car getAmbulance() {
        return new Ambulance();
    }

}
