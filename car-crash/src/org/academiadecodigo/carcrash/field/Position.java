package org.academiadecodigo.carcrash.field;

public class Position {
    private int row;
    private int col;

    public Position(int row, int column){
        this.row = row;
        this.col = column;
    }
    public int getCol() {
        return this.col;
    }

    public int getRow() {
        return this.row;
    }

    public void goSouth(){
        setPosition(this.row+1,this.col);
    }
    public void goNorth(){
        setPosition(this.row-1,this.col);
    }
    public void goEast(){
        setPosition(this.row,this.col+1);
    }
    public void goWest(){
        setPosition(this.row,this.col-1);
    }

    public Position getPosition(){
        return new Position(this.row,this.col);
    }
    public void setPosition(int row, int col){
        this.row = row;
        this.col = col;
    }
    public static Position generateRandomPosition(){
        int row, col;
        row = (int)Math.ceil(Math.random()*Field.getHeight()-1);
        col = (int)Math.ceil(Math.random()*Field.getWidth()-1);
        return new Position(row,col);
    }
}
