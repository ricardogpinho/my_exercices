package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class JDBCService implements UserService{

    private Connection connection;
    private Statement statement;

    public JDBCService(Connection connection) {

        this.connection = connection;
        try {
            this.statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } {
        }
    }

    @Override
    public boolean authenticate(String username, String password) {
        return false;
    }

    @Override
    public void add(User user) {
        try {
            statement.executeQuery("INSERT INTO user(username, email, password, firstname, lastname, phone) VALUES(" +
                    user.getUsername()+","+user.getEmail()+","+user.getPassword()+","+user.getFirstName()+","+user.getLastName()+","+user.getPhone());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public User findByName(String username) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }
}
