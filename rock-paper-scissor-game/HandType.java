public enum HandType {
	ROCK,
	PAPER,
	SCISSOR;

	public static HandType getRandomHand(){
		int handNumber = Randomizer.randomize(0,3);
		switch (handNumber){
			case 0: return HandType.ROCK;
			case 1: return HandType.PAPER;
			case 2: return HandType.SCISSOR;
		}
		return HandType.ROCK;
	}
}
