public class Playground{
	public static void main(String[] args){
		Player player_1 = new Player("P1");	
		Player player_2 = new Player("P2");	
		
		RockPaperScissor.start(player_1, player_2);
	}
}
