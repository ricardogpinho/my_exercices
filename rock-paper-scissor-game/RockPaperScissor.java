public class RockPaperScissor{
	public static void start(Player player_1, Player player_2){
		HandType player_1_hand = player_1.pickHand();
		HandType player_2_hand = player_2.pickHand();
		System.out.println("Player 1 hand is "+player_1_hand);
		System.out.println("Player 2 hand is "+player_2_hand);
		if(player_1_hand == HandType.ROCK){
			if(player_2_hand == HandType.SCISSOR){
				System.out.println("Player 1 WINS!");
			}else if (player_2_hand == HandType.PAPER){
				System.out.println("Player 2 WINS!");
			}else if (player_2_hand == HandType.ROCK){
				System.out.println("No one WINS!");
			}
		}else if(player_1_hand == HandType.PAPER){
			if(player_2_hand == HandType.SCISSOR){
				System.out.println("Player 2 WINS!");
			}else if (player_2_hand == HandType.ROCK){
				System.out.println("Player 1 WINS!");
			}else if (player_2_hand == HandType.PAPER){
				System.out.println("No one WINS!");
			}
		}else if(player_1_hand == HandType.SCISSOR){
			if(player_2_hand == HandType.PAPER){
				System.out.println("Player 1 WINS!");
			}else if (player_2_hand == HandType.ROCK){
				System.out.println("Player 2 WINS!");
			}else if (player_2_hand == HandType.SCISSOR){
				System.out.println("No one WINS!");
			}
		}
	}
	

}
