package org.academiadecodigo.felinux.mapeditor;

public class GlobalVariables {
    public static final int PADDING = 10;
    public static final int CELL_SIZE = 20;
    public static final int GRID_SIZE = 20;
}
