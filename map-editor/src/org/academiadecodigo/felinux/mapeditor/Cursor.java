package org.academiadecodigo.felinux.mapeditor;
import org.academiadecodigo.simplegraphics.graphics.Color;

import static org.academiadecodigo.felinux.mapeditor.GlobalVariables.*;
public class Cursor extends Cell{

    public Cursor(int col, int row) {
        super(col, row);
        super.cell.setColor(Color.GREEN);
        super.fill_cell();
    }

    public void moveUp(){
        if(this.row == 0)return;
        this.row -=1;
        this.cell.translate(0,-CELL_SIZE);
    }
    public void moveDown(){
        if(this.row == GRID_SIZE-1)return;
        this.row +=1;
        this.cell.translate(0, CELL_SIZE);
    }
    public void moveLeft(){
        if(this.col == 0)return;
        this.col -=1;
        this.cell.translate(-CELL_SIZE, 0);
    }
    public void moveRight(){
        if(this.col == GRID_SIZE-1)return;
        this.col +=1;
        this.cell.translate(CELL_SIZE,0);
    }
}
