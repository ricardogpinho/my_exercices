package org.academiadecodigo.felinux.mapeditor;
import static org.academiadecodigo.felinux.mapeditor.GlobalVariables.*;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.*;

public class MapEditor implements KeyboardHandler {

    Grid grid;
    Cursor cursor;
    Keyboard keyboard;
    FileReader load;
    BufferedReader br;
    FileWriter save;

    public MapEditor() {
        grid = new Grid(GRID_SIZE);
        cursor = new Cursor(0, 0);
        keyboard = new Keyboard(this);
        KeyboardInit();
    }

    public void init() {
        try {
            save = new FileWriter("src/org/academiadecodigo/felinux/mapeditor/save.txt");
            load = new FileReader("src/org/academiadecodigo/felinux/mapeditor/save.txt");
        }
        catch (FileNotFoundException ex){
            System.out.println("Couldn't read file");
        }
        catch (IOException ex){
            System.out.println("Couldn't write to file");
        }

        br = new BufferedReader(load);
        grid.draw();
    }

    public void loadGrid() {
        String temp = "";

        for (int i = 0; i < grid.getGrid_size(); i++) {
            try{
                temp = br.readLine();
                for(int j = 0; j<grid.getGrid_size(); j++) {
                    if (temp.charAt(j) == 'F') {
                        grid.cell_grid[j][i].fill_cell();
                    } else {
                        grid.cell_grid[j][i].draw_cell();
                    }
                }
            }catch (IOException ex){
                System.out.println("couldn't read line");
            }


            System.out.println(temp);
        }
    }


    public void saveGrid() {
        for(int i = 0; i<grid.getGrid_size();i++){
            for(int j=0;j<grid.getGrid_size(); j++){
                try {
                    if(grid.cell_grid[j][i].painted) {
                        save.write("F");
                    }else{
                        save.write("_");
                    }
                    }catch (IOException ex){
                        System.out.println("couldn't write character");
                }
            }
            try {
                save.write("\n");
            }catch (IOException ex){
                System.out.println("couldn't write new line");
            }
        }
        try {
            save.flush();
        }catch (IOException ex){
            System.out.println("couldn't close");
        }

    }


    public void KeyboardInit() {

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent s = new KeyboardEvent();
        s.setKey(KeyboardEvent.KEY_S);
        s.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent l = new KeyboardEvent();
        l.setKey(KeyboardEvent.KEY_L);
        l.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(space);
        keyboard.addEventListener(s);
        keyboard.addEventListener(l);

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case (KeyboardEvent.KEY_LEFT):
                cursor.moveLeft();
                break;
            case (KeyboardEvent.KEY_RIGHT):
                cursor.moveRight();
                break;
            case (KeyboardEvent.KEY_UP):
                cursor.moveUp();
                break;
            case (KeyboardEvent.KEY_DOWN):
                cursor.moveDown();
                break;
            case (KeyboardEvent.KEY_SPACE):
                grid.paint_cell(cursor.getCol(), cursor.getRow());
                break;
            case (KeyboardEvent.KEY_S):
                System.out.println("you pressed s");
                saveGrid();
                break;
            case (KeyboardEvent.KEY_L):
                System.out.println("you pressed l");
                loadGrid();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case (KeyboardEvent.KEY_SPACE):
                break;
        }
    }
}
