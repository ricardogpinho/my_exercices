package org.academiadecodigo.felinux.mapeditor;

public class Grid {


    private final int grid_size;
    Cell[][] cell_grid;

    public Grid(int grid_size){
        this.grid_size = grid_size;
        this.cell_grid = new Cell[grid_size][grid_size];
    }

    public void draw(){
        for(int i=0; i<grid_size;i++){
            for(int j=0;j<grid_size;j++){
                cell_grid[i][j] = new Cell(i,j);
                cell_grid[i][j].draw_cell();
            }
        }
    }

    public void paint_cell(int col, int row){
        cell_grid[col][row].paint_cell();
    }

    public int getGrid_size() {
        return grid_size;
    }
}
