package org.academiadecodigo.felinux.mapeditor;
import static org.academiadecodigo.felinux.mapeditor.GlobalVariables.*;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;


public class Cell {
    protected Rectangle cell;
    protected int col, row;
    protected boolean painted;

    public Cell(int col, int row){
        this.col = col;
        this.row = row;
        this.cell=new Rectangle(PADDING +col* CELL_SIZE,PADDING+row* CELL_SIZE, CELL_SIZE, CELL_SIZE);
    }

    public void fill_cell(){
        cell.fill();
        painted = true;
    }

    public void draw_cell(){
        cell.draw();
        painted = false;
    }

    public void paint_cell(){
        if(!painted){
            this.fill_cell();
            return;
        }
        this.draw_cell();
    }

    /*getters*/
    public int getCell_size() {
        return CELL_SIZE;
    }
    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

}
