public class User {
        private String username;
        private String password;

        protected User(String username, String password){
            this.username = username;
            this.password = password;
        }
        protected String getUsername(){
            return this.username;
        }
        protected String getPassword(){
            return this.password;
        }
}
