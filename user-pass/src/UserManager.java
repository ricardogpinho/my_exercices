import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.util.ArrayList;

public class UserManager {
    protected ArrayList<User> users;

    protected UserManager(){
        this.users = new ArrayList<User>();
        addUsers();
    }
    private void addUsers(){
        users.add(new User("Ricardo","1234"));
        users.add(new User("aaa","bbb"));
    }
    protected void login(){
        User userToLogin = UserManagerInterface.loginPrompt();
        for (User u: users) {
            if(userToLogin.getUsername().equals(u.getUsername()) &&
                    userToLogin.getPassword().equals(u.getPassword())){
                System.out.println("login successful.");
                return;
            }
        }
        System.out.println("login failed");
        login();
    }

    public static class UserManagerInterface {
        protected static User loginPrompt(){
            Prompt prompt = new Prompt(System.in,System.out);
            String username;
            String password;
            StringInputScanner stringInputScanner = new StringInputScanner();
            stringInputScanner.setMessage("Username: ");
            username = prompt.getUserInput(stringInputScanner);
            PasswordInputScanner passwordInputScanner = new PasswordInputScanner();
            passwordInputScanner.setMessage("Password: ");
            password = prompt.getUserInput(passwordInputScanner);
            return new User(username,password);
        }
    }

}
